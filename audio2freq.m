function audio2freq(filename)
% This function takes an audio file and plots it in the frequency domain.

% Get the audio file
[y,Fs] = audioread(filename);

% Transpose file
y = y';

% Fourier transform the audio
Y1 = fft(y(1,:));
Y1_fftshifted = abs(fftshift(Y1))/Fs;

% Create frequency vector
k = linspace(-Fs/2, Fs/2, length(Y1(1,:))+1);
k = k(1:end-1);

% Plot the audio
figure
plot(k, Y1_fftshifted)
hold on
title(sprintf('%s in the Frequency domain', filename))
xlabel('frequency (Hz)'), ylabel('magnitude')


% If audio has two channels
if size(y,1) > 1
    % Create and plot the right channel
    Y2 = fft(y(2,:));
    Y2_fftshifted = abs(fftshift(Y2))/Fs;
    plot(k, -Y2_fftshifted)
    legend('Left audio', 'Right audio');
end

end
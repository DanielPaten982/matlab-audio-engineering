function EGB240_output(audiofile)

% Define sample rate
sample_rate = 15625;

% Read in audio samples
[y,Fs] = audioread(audiofile);

% Convert audio to mono if currently multiple channels
if (size(y,2) == 2)
    y = sum(y,2) / size(y,2);
end

% Resample and rescale the audio
y_samp = resample(y, sample_rate, Fs);
y_out = 2.*(y_samp - min(y_samp))./(max(y_samp) - min(y_samp)) - 1;

% Write samples to wave file
audiowrite("EGB240.WAV", y_out, sample_rate, 'BitsPerSample', 8);

end